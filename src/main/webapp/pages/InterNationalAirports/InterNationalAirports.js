Application.$controller("InterNationalAirportsPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";
    $scope.selectedAirport = 'none';
    $scope.selectedCountry = 'none';
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.mobile_navbar1Backbtnclick = function($event, $isolateScope) {
        $scope.$emit('mobile_navbar1Backbtnclick');
    };

    $scope.IntlAirportsListTap = function($event, $isolateScope) {
        debugger
        if ($rootScope.airport == "from") {
            $scope.Variables.selectedFromIntlAirports.dataValue = $isolateScope.item;
            $scope.selectedAirport = $isolateScope.item.airportCode;
            $scope.selectedCountry = $isolateScope.item.country;
            //$scope.Variables.getDomesticAirports.setInput('selectedAirport', $isolateScope.item.airportCode);
            $scope.Variables.getIntlAirports.update();
            $scope.Variables.selectedToIntlAirports.dataValue = '';
        }
        if ($rootScope.airport == "to") {
            $scope.Variables.selectedToIntlAirports.dataValue = $isolateScope.item;
        }
        $scope.$emit('mobile_navbar1Backbtnclick');
    };

}]);