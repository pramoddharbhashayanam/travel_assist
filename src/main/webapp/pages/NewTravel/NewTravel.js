var airportType = 'domestic';
Application.$controller("NewTravelPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";



    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };

    function showSearch() {
        $scope.Widgets.mobile_navbar1.show = false;
        $scope.Widgets.segmented_control1.show = false;
        $scope.Widgets.container1.Widgets.mobile_navbar1.query = "";
        $scope.Widgets.container1.Widgets.mobile_navbar1.queryModel = "";
        $scope.Widgets.internationalContainer.Widgets.mobile_navbar1.query = "";
        $scope.Widgets.internationalContainer.Widgets.mobile_navbar1.queryModel = "";
        if (airportType == 'domestic') {
            $scope.Widgets.container1.show = true;
            $scope.Widgets.internationalContainer.show = false;
        } else
        if (airportType == 'international') {
            $scope.Widgets.internationalContainer.show = true;
            $scope.Widgets.container1.show = false;
        }

    }


    $scope.$on('mobile_navbar1Backbtnclick', function() {
        $scope.Widgets.mobile_navbar1.show = true;
        $scope.Widgets.segmented_control1.show = true;
        $scope.Widgets.container1.show = false;
        $scope.Widgets.internationalContainer.show = false;

    });



    $scope.fromAirportTextFieldFocus = function($event, $isolateScope) {
        $rootScope.airport = "from";
        showSearch();

    };


    $scope.toAirportTextFieldTap = function($event, $isolateScope) {
        $rootScope.airport = "to";
        showSearch();

    };




    $scope.segmented_control1Segmentchange = function($isolateScope, $old, $new) {

        if ($new === 1) {
            airportType = 'international';
        }
        if ($new === 0) {
            airportType = 'domestic';
        }
    };


    $scope.label19Tap = function($event, $isolateScope) {
        $scope.fromAirportTextFieldFocus();
    };

    function checkData(data) {
        if (data.tripName === undefined || data.destinationsByFrom === "") {
            $scope.Variables.errorNotify.dataBinding.text = "Enter Trip Name";
            return false;
        }
        if (data.destinationsByFrom === "" || data.destinationsByFrom === undefined) {
            $scope.Variables.errorNotify.dataBinding.text = "Select Source Airport";
            return false;
        }
        if (data.destinationsByTo === "" || data.destinationsByTo === undefined) {
            $scope.Variables.errorNotify.dataBinding.text = "Select Destination Airport";
            return false;
        } else {
            return true;
        }
    }


    $scope.createIntlReqonBeforeUpdate = function(variable, data) {
        if (!checkData(data)) {
            $scope.Variables.errorNotify.notify();
            return false;
        }
    };


    $scope.createRequestonBeforeUpdate = function(variable, data) {
        if (!checkData(data)) {
            $scope.Variables.errorNotify.notify();
            return false;
        }
    };


    $scope.createRequestonSuccess = function(variable, data) {
        $scope.Variables.selectedFromAirport.dataValue = "";
        $scope.Variables.selectedToAirport.dataValue = "";
    };


    $scope.createIntlReqonSuccess = function(variable, data) {
        $scope.Variables.selectedFromIntlAirports.dataValue = "";
        $scope.Variables.selectedToIntlAirports.dataValue = "";
    };

}]);

Application.$controller("stay_selection_dialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        function staySelect(selectedStay) {
            if (airportType == 'domestic')
                $scope.Widgets.stayButton.caption = selectedStay;
            if (airportType == 'international')
                $scope.Widgets.IntlstayButton.caption = selectedStay;
            DialogService.hideDialog("stay_selection_dialog");
        }


        $scope.closeToAirportButtonTap = function($event, $isolateScope) {
            staySelect("Close to Airport");
        };


        $scope.closeToBButtonTap = function($event, $isolateScope) {
            staySelect("Close to Business");
        };


        $scope.noHotelBookingButtonTap = function($event, $isolateScope) {
            staySelect(" No Hotel Booking");
        };

    }
]);

Application.$controller("class_selection_dialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        function classSelect(selectedClass) {
            if (airportType == 'domestic')
                $scope.Widgets.classButton.caption = selectedClass;
            if (airportType == 'international')
                $scope.Widgets.IntlclassButton.caption = selectedClass;

            DialogService.hideDialog("class_selection_dialog");
        }


        $scope.economyButtonTap = function($event, $isolateScope) {

            classSelect("Economy");

        };


        $scope.premiumButtonTap = function($event, $isolateScope) {

            classSelect("Premium Economy");
        };


        $scope.businessButtonTap = function($event, $isolateScope) {
            classSelect("Business");
        };

    }
]);

Application.$controller("billing_selection_dialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        function billingSelect(selectedBilling) {

            if (airportType == 'domestic') {
                $scope.Widgets.billingButton.caption = selectedBilling;
            } else
            if (airportType == 'international') {
                $scope.Widgets.IntlbillingsdButton.caption = selectedBilling;
            }
            DialogService.hideDialog("billing_selection_dialog");
        }

        $scope.clientButtonTap = function($event, $isolateScope) {
            billingSelect("Client");
        };


        $scope.companyBButtonTap = function($event, $isolateScope) {
            billingSelect("Company");
        };


        $scope.employeeButtonTap = function($event, $isolateScope) {
            billingSelect("Employee");
        };

    }
]);

Application.$controller("visa_selection_dialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        function visaSelect(selectedVisa) {

            if (airportType == 'domestic') {
                $scope.Widgets.visaButton.caption = selectedVisa;
            } else
            if (airportType == 'international') {
                $scope.Widgets.visaButton.caption = selectedVisa;
            }
            DialogService.hideDialog("visa_selection_dialog");
        }


        $scope.l1ButtonTap = function($event, $isolateScope) {
            visaSelect("L1");
        };


        $scope.h1bButtonTap = function($event, $isolateScope) {
            visaSelect("H1-B");
        };


        $scope.b1b2ButtonTap = function($event, $isolateScope) {
            visaSelect("B1/B2");
        };

    }
]);