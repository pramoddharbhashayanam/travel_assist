/*Copyright (c) 2015-2016 wavemaker-com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker-com*/
package com.travel_advisor.travel_advisor_db.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.travel_advisor.travel_advisor_db.Requests;


/**
 * ServiceImpl object for domain model class Requests.
 *
 * @see Requests
 */
@Service("TRAVEL_ADVISOR_DB.RequestsService")
public class RequestsServiceImpl implements RequestsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestsServiceImpl.class);


    @Autowired
    @Qualifier("TRAVEL_ADVISOR_DB.RequestsDao")
    private WMGenericDao<Requests, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Requests, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "TRAVEL_ADVISOR_DBTransactionManager")
    @Override
	public Requests create(Requests requests) {
        LOGGER.debug("Creating a new Requests with information: {}", requests);
        Requests requestsCreated = this.wmGenericDao.create(requests);
        return requestsCreated;
    }

	@Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public Requests getById(Integer requestsId) throws EntityNotFoundException {
        LOGGER.debug("Finding Requests by id: {}", requestsId);
        Requests requests = this.wmGenericDao.findById(requestsId);
        if (requests == null){
            LOGGER.debug("No Requests found with id: {}", requestsId);
            throw new EntityNotFoundException(String.valueOf(requestsId));
        }
        return requests;
    }

    @Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public Requests findById(Integer requestsId) {
        LOGGER.debug("Finding Requests by id: {}", requestsId);
        return this.wmGenericDao.findById(requestsId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public Requests update(Requests requests) throws EntityNotFoundException {
        LOGGER.debug("Updating Requests with information: {}", requests);
        this.wmGenericDao.update(requests);

        Integer requestsId = requests.getRequestId();

        return this.wmGenericDao.findById(requestsId);
    }

    @Transactional(value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public Requests delete(Integer requestsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Requests with id: {}", requestsId);
        Requests deleted = this.wmGenericDao.findById(requestsId);
        if (deleted == null) {
            LOGGER.debug("No Requests found with id: {}", requestsId);
            throw new EntityNotFoundException(String.valueOf(requestsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public Page<Requests> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Requests");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
    @Override
    public Page<Requests> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Requests");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service TRAVEL_ADVISOR_DB for table Requests to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "TRAVEL_ADVISOR_DBTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }



}

